"use strict";

var puppeteer = require('puppeteer');

require('dotenv').config(); // Import the Google Cloud Vision library


var vision = require('@google-cloud/vision'); // const { time } = require('node:console');
// Create a client


var client = new vision.ImageAnnotatorClient();
var stingCaptcha = "";
var times = 2;

(function _callee() {
  var browser, page, element, dataReturn, captcha, elements, loop, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _iterator, _step, handle, getThemAll, videoLoop, start, popupArr, _iteratorNormalCompletion2, _didIteratorError2, _iteratorError2, _iterator2, _step2, handle2;

  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return regeneratorRuntime.awrap(puppeteer.launch({
            headless: true
          }));

        case 2:
          browser = _context.sent;
          _context.next = 5;
          return regeneratorRuntime.awrap(browser.newPage());

        case 5:
          page = _context.sent;

        case 6:
          if (!(stingCaptcha == "")) {
            _context.next = 28;
            break;
          }

          if (!(times < 0)) {
            _context.next = 10;
            break;
          }

          _context.next = 10;
          return regeneratorRuntime.awrap(browser.close());

        case 10:
          _context.next = 12;
          return regeneratorRuntime.awrap(page["goto"]('https://www.adworld.company/'));

        case 12:
          _context.next = 14;
          return regeneratorRuntime.awrap(page.waitForSelector('#ctl00_ContentPlaceHolder1_Captcha_IMG'));

        case 14:
          _context.next = 16;
          return regeneratorRuntime.awrap(page.$('#ctl00_ContentPlaceHolder1_Captcha_IMG'));

        case 16:
          element = _context.sent;
          _context.next = 19;
          return regeneratorRuntime.awrap(delay(1000));

        case 19:
          _context.next = 21;
          return regeneratorRuntime.awrap(element.screenshot({
            path: 'images/captcha.png'
          }));

        case 21:
          _context.next = 23;
          return regeneratorRuntime.awrap(imateToText());

        case 23:
          dataReturn = _context.sent;
          if (stingCaptcha.length < 5 || !dataReturn) stingCaptchan = '';
          times--;
          _context.next = 6;
          break;

        case 28:
          /// login ///
          //  console.log(stingCaptcha);
          captcha = stingCaptcha; //  console.log(captcha)

          _context.next = 31;
          return regeneratorRuntime.awrap(page.$eval('#ctl00_ContentPlaceHolder1_txtUsername', function (el) {
            return el.value = 'jakkritt26@gmail.com';
          }));

        case 31:
          _context.next = 33;
          return regeneratorRuntime.awrap(page.$eval('#ctl00_ContentPlaceHolder1_txtPassword', function (el) {
            return el.value = 'jj053441736';
          }));

        case 33:
          _context.next = 35;
          return regeneratorRuntime.awrap(Promise.all([page.type('#ctl00_ContentPlaceHolder1_txtCapcha', captcha, {
            delay: 500
          }), //page.click(SELECTOR.btnSubmit),
          page.waitForNavigation({
            waitUntil: 'networkidle0'
          })]));

        case 35:
          _context.next = 37;
          return regeneratorRuntime.awrap(page.$$('.Walletbtn4'));

        case 37:
          elements = _context.sent;
          loop = 1;
          _iteratorNormalCompletion = true;
          _didIteratorError = false;
          _iteratorError = undefined;
          _context.prev = 42;
          _iterator = elements[Symbol.iterator]();

        case 44:
          if (_iteratorNormalCompletion = (_step = _iterator.next()).done) {
            _context.next = 53;
            break;
          }

          handle = _step.value;

          if (!(loop == 2)) {
            _context.next = 49;
            break;
          }

          _context.next = 49;
          return regeneratorRuntime.awrap(handle.click());

        case 49:
          loop++;

        case 50:
          _iteratorNormalCompletion = true;
          _context.next = 44;
          break;

        case 53:
          _context.next = 59;
          break;

        case 55:
          _context.prev = 55;
          _context.t0 = _context["catch"](42);
          _didIteratorError = true;
          _iteratorError = _context.t0;

        case 59:
          _context.prev = 59;
          _context.prev = 60;

          if (!_iteratorNormalCompletion && _iterator["return"] != null) {
            _iterator["return"]();
          }

        case 62:
          _context.prev = 62;

          if (!_didIteratorError) {
            _context.next = 65;
            break;
          }

          throw _iteratorError;

        case 65:
          return _context.finish(62);

        case 66:
          return _context.finish(59);

        case 67:
          _context.next = 69;
          return regeneratorRuntime.awrap(Promise.all([page.waitForNavigation()]));

        case 69:
          _context.next = 71;
          return regeneratorRuntime.awrap(page.$$('div.col-lg-3 .pt-10 img'));

        case 71:
          getThemAll = _context.sent;
          videoLoop = 3;
          start = 1;
          popupArr = [];
          _iteratorNormalCompletion2 = true;
          _didIteratorError2 = false;
          _iteratorError2 = undefined;
          _context.prev = 78;
          _iterator2 = getThemAll[Symbol.iterator]();

        case 80:
          if (_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done) {
            _context.next = 105;
            break;
          }

          handle2 = _step2.value;
          console.log('start');

          if (!(videoLoop <= 0)) {
            _context.next = 85;
            break;
          }

          return _context.abrupt("return");

        case 85:
          _context.next = 87;
          return regeneratorRuntime.awrap(handle2.click());

        case 87:
          _context.next = 89;
          return regeneratorRuntime.awrap(delay(1000));

        case 89:
          if (!(start == 6 || videoLoop == 0)) {
            _context.next = 95;
            break;
          }

          _context.next = 92;
          return regeneratorRuntime.awrap(delay(40000));

        case 92:
          //  popupArr.forEach(async element => {
          //     console.log(element);
          //      await element.close();
          //    });
          popupArr = [];
          console.log('close');
          start = 1;

        case 95:
          console.log('Video Click :', videoLoop);
          videoLoop--;
          start++;

          if (!(videoLoop <= 0)) {
            _context.next = 102;
            break;
          }

          console.log('close');
          _context.next = 102;
          return regeneratorRuntime.awrap(browser.close());

        case 102:
          _iteratorNormalCompletion2 = true;
          _context.next = 80;
          break;

        case 105:
          _context.next = 111;
          break;

        case 107:
          _context.prev = 107;
          _context.t1 = _context["catch"](78);
          _didIteratorError2 = true;
          _iteratorError2 = _context.t1;

        case 111:
          _context.prev = 111;
          _context.prev = 112;

          if (!_iteratorNormalCompletion2 && _iterator2["return"] != null) {
            _iterator2["return"]();
          }

        case 114:
          _context.prev = 114;

          if (!_didIteratorError2) {
            _context.next = 117;
            break;
          }

          throw _iteratorError2;

        case 117:
          return _context.finish(114);

        case 118:
          return _context.finish(111);

        case 119:
        case "end":
          return _context.stop();
      }
    }
  }, null, null, [[42, 55, 59, 67], [60,, 62, 66], [78, 107, 111, 119], [112,, 114, 118]]);
})();

function delay(time) {
  return new Promise(function (resolve) {
    setTimeout(resolve, time);
  });
}

function imateToText() {
  return regeneratorRuntime.async(function imateToText$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return regeneratorRuntime.awrap(client.textDetection('images/captcha.png').then(function (results) {
            var result = results[0].fullTextAnnotation;
            var format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
            if (!result) return false;
            if (result.text.length < 5) return false;
            stingCaptcha = result.text;
            if (format.test(stingCaptcha)) return false;
            console.log('Text Annotations Result:', result.text);
          })["catch"](function (err) {
            console.error('ERROR:', err);
          }));

        case 2:
        case "end":
          return _context2.stop();
      }
    }
  });
}