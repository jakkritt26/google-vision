const puppeteer = require("puppeteer");
const cron = require("node-cron");
const express = require("express");
const vision = require("@google-cloud/vision");
const nodemailer = require("nodemailer");
const fs = require("fs");
require("dotenv").config();

let stingCaptcha = "";
const videoClick = 6;
let videoLoopRem = 0;

async function autoclick(email_rec, password_rec) {
  let rawdata = fs.readFileSync("data/data.json");
  let dataGet = JSON.parse(rawdata);

  const dateObj = new Date();
  const month = dateObj.getMonth();

  const day = String(dateObj.getDate()).padStart(2, "0");
  const year = dateObj.getFullYear();
  const dOutput = year + "-" + month + "-" + day;

  for (let log of dataGet.logs) {
    if (log.email === email && dOutput === log.date) {
      console.log("succcess today");
      return false;
    }
  }

  if (email_rec) email = email_rec;
  if (password_rec) password = password_rec;

  let times = 2;
  // (async () => {
  const browser = await puppeteer.launch({
    headless: true,
    args: ["--no-sandbox", "--disable-setuid-sandbox"],
  });
  const page = await browser.newPage();

  /// alert box //

  await page.on("dialog", async (dialog) => {
    console.log(dialog.message());
    await dialog.dismiss();
    try {
      await page.close();
      await browser.close();
      return false;
    } catch (error) {
      console.log(error);
    }
  });

  /// Load image //
  await delay(2000);
  console.log("get captcha");
  // logdata.push('get captcha')
  while (stingCaptcha == "") {
    if (times < 0) {
      await page.close();
      await browser.close();
      return false;
    }
    await page.goto("https://www.adworld.company/");
    await page.waitForSelector("#ctl00_ContentPlaceHolder1_Captcha_IMG");
    const element = await page.$("#ctl00_ContentPlaceHolder1_Captcha_IMG");
    await delay(1000);
    await element.screenshot({
      path: "images/captcha.png",
    });
    const dataReturn = await imateToText();
    // stingCaptcha ='1111'
    if (stingCaptcha.length < 5 || !dataReturn) stingCaptchan = "";
    times--;
  }

  /// login ///
  const captcha = await stingCaptcha;

  try {
    if (captcha == "") {
      await page.close();
      await browser.close();
      return false;
    }
  } catch (error) {}

  //  console.log(captcha)
  await page.$eval(
    "#ctl00_ContentPlaceHolder1_txtUsername",
    (el, value) => (el.value = value),
    email
  );
  await page.$eval(
    "#ctl00_ContentPlaceHolder1_txtPassword",
    (el, value) => (el.value = value),
    password
  );

  console.log("logining");

  await Promise.all([
    page.type("#ctl00_ContentPlaceHolder1_txtCapcha", captcha, {
      delay: 500,
      //page.click(SELECTOR.btnSubmit),
    }),
    page
      .waitForNavigation({
        waitUntil: "networkidle0",
        timeout: 0,
      })
      .then(function (e) {}),
  ]).then(function (result) {
    //console.log('1')
  });

  console.log("logined");
  // logdata.push('logined')

  //await page.goto('https://www.adworld.company/');
  //await page.click("input[name='alert']")
  // logdata.push('dashboard')
  try {
    await page.$("#dialog");
    await page.click(".close");
  } catch (error) {}
  console.log("dashboard");
  await delay(2000);

  const elements = await page.$$(".Walletbtn4");
  let loop = 1;
  for (const handle of elements) {
    if (loop == 2) await handle.click();
    loop++;
  }

  await Promise.all([
    page.waitForNavigation({
      waitUntil: "networkidle0",
      timeout: 0,
    }),
  ]).then(function (result) {
    //console.log('1')
  });
  // await watchVideo(page, browser, email)
  console.log("watch Vdo");

  //console.log(vdoElement)
  //const videoview = vdoElement.splite('/')
  // console.log(videoview)
  let getThemAll = await page.$$("div.col-lg-3 .pt-10 img");

  //await page.waitForSelector('#ctl00_ContentPlaceHolder1_txtTodayVideoWatch')
  const text = await page.$("#ctl00_ContentPlaceHolder1_txtTodayVideoWatch");
  let vdoElement = await text.getProperty("innerText");
  vdoElement = await vdoElement.jsonValue();
  let videoCount = await vdoElement.split("/");
  // console.log(vdoElement)
  let videoLoop = await parseInt(videoCount[0]);
  let videoLoopAll = await parseInt(videoCount[1]);
  if (videoLoop == videoLoopAll) {
    await dataGet.logs.push({
      email: email,
      date: dOutput,
    });
    let datas = JSON.stringify(dataGet);
    fs.writeFileSync("data/data.json", datas);
    console.log("success");

    try {
      const lineNotify = require("line-notify-nodejs")(
        "JVPXZFKsirzMMayT7cn3QMbOpCBLDD7ydDkKiOtb9ru"
      );

      lineNotify
        .notify({
          message: "Success : Email " + email + " Run " + videoLoop + " Today.",
        })
        .then(() => {
          console.log("send completed!");
        });
    } catch (error) {
      console.log(error);
    }

    await browser.close();
    return false;
  }

  // if(videoLoop)
  videoLoop = await (videoLoopAll - videoLoop);
  videoLoop = await (videoLoop + Math.floor(videoLoop / 10));
  let start = 0;

  try {
    if (videoLoop == 0) {
      getThemAll = await [];
      await browser.close();
      return false;
    }
  } catch (error) {}

  await delay(2000);
  console.log("start watch Vdo");
  if (!getThemAll) {
    console.log("No Video close browser");
    await browser.close();
    return false;
  }
  for (const handle2 of getThemAll) {
    console.log("start");
    // logdata.push('start')
    console.log("Video Click :", videoLoop);
    // logdata.push('Video Click :'+videoLoop)
    await handle2.click();
    videoLoop--;
    start++;

    await delay(1000);
    if (start % videoClick === 0 || videoLoop == 0) {
      console.log("wait 60 sec");
      const pages = await browser.pages();

      await delay(60000);
      //console.log('page close')
      for (let i = pages.length - 1; i > 2; i--) await pages[i].close();
      popupArr = [];
      start = 0;
    }

    if (videoLoop <= 0) {
      //fs.writeFile(fileName, logdata)
      //sendMail(email)

      console.log("close browser");
      await browser.close();
      return true;
    }
  }
}

const clearLogs = (dataGet) => {};

function delay(time) {
  return new Promise(async function (resolve) {
    await setTimeout(resolve, time);
  });
}

async function imateToText() {
  const client = new vision.ImageAnnotatorClient();
  await client
    .textDetection("images/captcha.png")
    .then((results) => {
      const result = results[0].fullTextAnnotation;
      const format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
      if (!result) return false;
      if (result.text.length < 5) return false;
      stingCaptcha = result.text;
      if (format.test(stingCaptcha)) return false;
      console.log("Text Annotations Result:", result.text);
    })
    .catch((err) => {
      console.error("ERROR:", err);
    });
}

const sendMail = async (emailTo) => {
  // let mail = nodemailer.createTransport({
  //   service: 'gmail',
  //   auth: {
  //     user: 'jakkrit.prasert26@gmail.com',
  //     pass: 'jj053441736',
  //   },
  // })

  let mail = nodemailer.createTransport({
    host: "smtp.sendgrid.net",
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
      // ข้อมูลการเข้าสู่ระบบ
      user: "apikey", // email user ของเรา
      pass:
        "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", // email password
    },
  });

  const dNow = new Date();
  const mailOptions = {
    from: "mailer@jdev.click",
    to: emailTo,
    subject: "Auto Click " + emailTo,
    html: "Auto Click " + emailTo + " finish at" + dNow,
  };

  try {
    mail.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log("Email sent: " + info.response);
      }
    });
  } catch (error) {
    console.log(error);
  }
};

module.exports = {
  autoclick,
  // delay,
  // imateToText
};

const args = require("minimist")(process.argv.slice(2));
email = args["email"];
password = args["password"];
autoclick(email, password);

//autoclick('jakkritt26@gmail.com', 'jj053441736')
//autoclick('jaidee.development@gmail.com', 'jj053441736')
