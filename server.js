const express = require("express");
const { spawn } = require("child_process");
const app = express();
var users = require("./routes/user.js");
// ...

const fs = require("fs");
const port = 3000;
// const cron = require('node-cron');
var cronJob = require("cron").CronJob;

/// round 1 ///
let loop = 0;
var cronJ = new cronJob(
  "0 */17 10-23 * * 0-6",
  async function () {
    let user = new Array();
    /// reaad file ///

    let rawdata = fs.readFileSync("data/user.json");
    let dataGet = JSON.parse(rawdata);

    for (let data of dataGet.user) {
      user.push(
        "node index.js --email=" + data.email + " --password=" + data.password
      );
    }

    const date = new Date();
    console.log(user[loop], date);
    let options = {
      shell: true,
    };
    let ls = spawn(user[loop], null, options);
    setTimeout(function () {
      console.log("kill");
      ls.stdin.pause();
      ls.kill();
    }, 900000);

    ls.stdout.on("data", (data) => {
      console.log(`stdout: ${data}`);
    });

    ls.stderr.on("data", (data) => {
      console.log(`stderr: ${data}`);
    });

    ls.on("error", (error) => {
      console.log(`error: ${error.message}`);
    });

    ls.on("close", (code) => {
      console.log(`child process exited with code ${code}`);
    });
    loop++;
    if (loop == user.length) loop = 0;
  },
  undefined,
  true,
  "Asia/Bangkok"
);

app.use("/user", users);

app.get("/", (req, res) => {
  // autoclick()
  res.send("Running");
});

// app.get('/autoclick/:email/:password', async (req, res) => {
//   if (req.params.email != 'jakkritt26@gmail.com' && req.params.email != 'jaidee.development@gmail.com') return false
//   const password = req.params.password
//   await autoclass.autoclick(req.params.email, password)
//   res.send('Success')
// })

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
