var express = require("express");
var router = express.Router();
var userController = require("../controller/userController");

router.post("/auth", userController.auth);

router.get("/", userController.getUser);

router.post("/", userController.addUser);

router.patch("/", userController.updateUser);

router.delete("/", userController.deleteUser);

module.exports = router;
